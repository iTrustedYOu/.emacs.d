(global-set-key [(control shift X)] 'clipboard-kill-region)
(global-set-key [(control shift C)] 'clipboard-kill-ring-save)
(global-set-key [(control shift V)] 'clipboard-yank)
(global-set-key (kbd "C-?") 'comment-or-uncomment-region)
(global-set-key (kbd "C-x f") 'recentf-open-files )
(global-set-key (kbd "C-c <left>") 'winner-undo)
(global-set-key (kbd "C-c <right>") 'winner-redo)
(global-set-key (kbd "C-c h") 'windmove-left)
(global-set-key (kbd "C-c l") 'windmove-right)
(global-set-key (kbd "C-c k") 'windmove-up)
(global-set-key (kbd "C-c j") 'windmove-down)
(global-set-key (kbd "C-c C-m")'execute-extended-command)
(global-set-key (kbd "C-x C-m")'execute-extended-command)
(global-set-key (kbd "C-x e") 'end-of-buffer)
(global-set-key (kbd "C-x t") 'beginning-of-buffer)
(global-set-key (kbd "C-x C-b") 'ibuffer)
(global-set-key (kbd "C-c b" ) 'burry-buffer)
(defalias 'qrr 'query-replace-regexp)

