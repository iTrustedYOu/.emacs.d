;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defconst dotfiles-dir "~/.emacs.d")
(require 'package)
(add-to-list 'package-archives
             '("marmalade" . "http://marmalade-repo.org/packages/") t)
(add-to-list 'package-archives
  '("melpa" . "http://melpa.milkbox.net/packages/") t)
(when (not package-archive-contents)
  (package-refresh-contents))
(package-initialize)

;; Add in your own as you wish:
(defvar my-packages '(starter-kit starter-kit-lisp starter-kit-bindings)
  "A list of packages to ensure are installed at launch.")

(dolist (p my-packages)
  (when (not (package-installed-p p))
    (package-install p)))




(add-to-list 'custom-theme-load-path (concat dotfiles-dir "/themes" ))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes (quote ("d677ef584c6dfc0697901a44b885cc18e206f05114c8a3b7fde674fce6180879" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" "e16a771a13a202ee6e276d06098bc77f008b73bbac4d526f160faa2d76c1dd0e" "f5e56ac232ff858afb08294fc3a519652ce8a165272e3c65165c42d6fe0262a0" "f641bdb1b534a06baa5e05ffdb5039fb265fde2764fbfd9a90b0d23b75f3936b" default)))
 '(send-mail-function nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;; JEDI FOR PYTHON ;;;;;;;;;;;;;;

(require 'epc)
(add-hook 'python-mode-hook 'jedi:setup)
(setq jedi:complete-on-dot t)
(eval-after-load "python"
  '(define-key python-mode-map "\C-cx" 'jedi-direx:pop-to-buffer))
;; (require 'auto-complete )
;; (autoload 'jedi:setup "jedi" nil t)
;; (add-hook 'python-mode-hook 'jedi:setup)
;; (setq jedi:setup-keys t)
;; (setq jedi:install-imenu t)
;; (setq jedi:imenu-create-index-function 'jedi:create-flat-imenu-index)
;; (setq jedi:complete-on-dot t)
;; (add-hook 'ein:connect-mode-hook 'ein:jedi-setup)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;EIN;;;;;;;;;;;;;;;;;;;;;;;
(require 'ein)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; EMACS FOR PYTHON ;;;;;;;;;;;;

(add-to-list 'load-path (concat dotfiles-dir "/emacs-for-python") )
(require 'epy-setup)

(require 'epy-python)
(require 'epy-editing)
(epy-setup-ipython)
(require 'highlight-indentation)
(add-hook 'python-mode-hook 'highlight-indentation)



(add-to-list 'load-path (concat dotfiles-dir "/vendor"))
(add-to-list 'load-path (concat dotfiles-dir "/themes"))
(add-to-list 'load-path (concat dotfiles-dir "/themes/tomorrow-theme/GNU\ Emacs/"))
(require 'color-theme-tomorrow)
;; (color-theme-tomorrow--define-theme night-eighties)
(color-theme-tomorrow--define-theme day)







;;rainbow mode is cool
(rainbow-mode t)
(add-to-list 'load-path (concat dotfiles-dir "/zencoding"))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;ZENCODING MODE;;;;;;;;;;;;;;;;;;;
(require 'zencoding-mode)
(add-hook 'sgml-mode-hook 'zencoding-mode)
(add-hook 'html-mode-hook 'zencoding-mode)
(add-hook 'css-mode-hook 'zencoding-mode)
(set-face-attribute 'default nil :height 100)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; undo and redo with C-c right-arrow and C-c left-arrow
(winner-mode 1)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;WORKGROUPS;;;;;;;;;;;;;;;;;;;
(require 'windata)
(require 'dirtree)
(add-to-list 'load-path (concat dotfiles-dir "/vendor/workgroups.el"))
(require 'workgroups)
(setq wg-prefix-key (kbd "C-c w"))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;BEEP SLOW REGEX QUERY REPLACE;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun beep ()  (play-sound-file "/usr/share/sounds/gnome/default/alerts/sonar.wav" ))

(defun slow-find-replace-with-sound()
  (progn
   (sit-for 1)
   (beep)
   "join")
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;C++ MODE;;;;;;;;;;;;;;;;;;
(require 'semantic/ia)
(require 'semantic/bovine/gcc)
(add-hook 'c++-mode-hook 'semantic-mode)
(add-hook 'c-mode-hook 'semantic-mode)
(defun my-semantic-hook ()
  (imenu-add-to-menubar "TAGS"))
(add-hook 'semantic-init-hooks 'my-semantic-hook)
(defun my-c-mode-cedet-hook ()
 (local-set-key "." 'semantic-complete-self-insert)
 (local-set-key ">" 'semantic-complete-self-insert)
 (local-set-key (kbd "C-.") 'semantic-ia-fast-jump)
 (local-set-key (kbd "C-M-g") 'semantic-symref)
 (add-to-list 'ac-sources 'ac-source-gtags)
 (add-to-list 'ac-sources 'ac-source-semantic))
(add-hook 'c++-mode-hook 'my-c-mode-cedet-hook)
(add-hook 'c-mode-hook 'my-c-mode-cedet-hook)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;EMAIL;;;;;;;;;;;;;;;;;;

(setq send-mail-function 'smtpmail-send-it
      message-send-mail-function 'smtpmail-send-it
      smtpmail-starttls-credentials
      '(("smtp.gmail.com" 587 nil nil))
      smtpmail-auth-credentials
      (expand-file-name "~/.authinfo")
      smtpmail-default-smtp-server "smtp.gmail.com"
      smtpmail-smtp-server "smtp.gmail.com"
      smtpmail-smtp-service 587
      smtpmail-debug-info t)
(require 'smtpmail)





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;JS2 MODE;;;;;;;;;;;;;;;;;
(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
(setq ac-js2-evaluate-calls t)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;JQUERY;;;;;;;;;;;;;;;;;;;;;

(add-to-list 'load-path (concat dotfiles-dir "/jquery-doc"))
(require 'jquery-doc)
(add-hook 'js2-mode-hook 'jquery-doc-setup)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;MULTI-WEB_MODE;;;;;;;;;;;;

 (require 'multi-web-mode)
   (setq mweb-default-major-mode 'html-mode)
   (setq mweb-tags '((php-mode "<\\?php\\|<\\? \\|<\\?=" "\\?>")
                      (js2-mode "<script +\\(type=\"text/javascript\"\\|language=\"javascript\"\\)[^>]*>" "</script>")
                      (css-mode "<style +type=\"text/css\"[^>]*>" "</style>")))
   (setq mweb-filename-extensions '("php" "htm" "html" "ctp" "phtml" "php4" "php5"))
(multi-web-global-mode 1)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;; SASS ;;;;;;;;;;;;;;;;;;
(setq exec-path (cons "/usr/local/bin/sass" exec-path))
(setq exec-path (cons "/usr/local/bin" exec-path))
(add-to-list 'load-path (concat dotfiles-dir "/scss-mode"))
(autoload 'scss-mode "scss-mode")
(add-to-list 'auto-mode-alist '("\\.scss\\'" . scss-mode))
(add-hook 'scss-mode-hook 'zencoding-mode)
(add-hook 'scss-mode-hook 'rainbow-mode)
(add-hook 'scss-mode-hook 'hl-line-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;; MULTI-TERM ;;;;;;;;;;;;;;
(setq multi-term-program "/bin/bash")




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;; DIRED;;;;;;;;;;;;;;;;;;;
(global-set-key (kbd "S-<f1>")
                (lambda ()
                  (interactive)
                  (dired "~/repos/")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;DJANGO MODE;;;;;;;;;;;;;;;;;;
(add-to-list 'load-path (concat dotfiles-dir "/django-mode"))
(require 'django-html-mode)
(require 'django-mode )
(yas/load-directory "~/.emacs.d/django-mode/snippets/")
(add-to-list 'auto-mode-alist '("\\.djhtml$" . django-html-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml$" . zencoding-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;MATCH PAREN, LIKE VI;;;;;;;;;;;
;; (global-set-key "%" 'match-paren
;;                 )
;; (defun match-paren (arg)
;;   "Go to matching paren if on paren, otherwise insert %."
;;   (interactive "p")
;;   (cond ((looking-at "\\s\(")) (forward-list 1) (backward-char 1))
;;   (cond ((looking-at "\\s\)")) (forward-list 1) (backward-char 1))
;;   (cond ((looking-at "\\s\{")) (forward-list 1) (backward-char 1))
;;   (cond ((looking-at "\\s\}")) (forward-list 1) (backward-char 1))
;;   (cond ((looking-at "\\s\[")) (forward-list 1) (backward-char 1))
;;   (cond ((looking-at "\\s\[")) (forward-list 1) (backward-char 1))
;;   (t (self-insert-command (or arg ))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;; HIGHLIGHTED LINE ;;;;;;;;;;;;;;
(set-face-background 'hl-line "#fff453")
(set-face-background 'hl-line "#333333")










