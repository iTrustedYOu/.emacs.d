/*
 * Copyright (c) 2011 Junichiro Okuno
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and should not be interpreted as representing official policies, either expressed
 * or implied, of Junichiro Okuno.
 */

package tcp_net.management;

import tcp_net.CommandAnalyser;
import tcp_net.management.commands.*;
import tcp_net.management.user_account.UserAccountManager;
import java.io.*;

import java.util.List;
import java.util.ArrayList;

/**
	This class provides to translate user inputs as a string text format to defined user commands
	@author J. Okuno
*/
public class UserCommandProcesssor
{
	/**
		An email server manager
	*/
	private EmailServerManager email_manager;
	
	/**
		A user account manager
	*/
	private UserAccountManager ua_manager; 
	
	/**
		Init a user command processor, creating a new email server manager
	*/
	public UserCommandProcesssor() 
	{ 
		email_manager = new EmailServerManager(3, 1); 
		ua_manager = email_manager.getUserAccountManager();
	}
	
	/**
		This method provides to execute a user command
		@param line a command request with arguments from a user
		@return a list of responses from a selected command in a string text format
	*/
	public List<String> process(String line) 
	{
		CommandAnalyser analyser = new CommandAnalyser();
		return process(analyser.getCommand(line), analyser.getArguments(line));
	}
	
	/**
		This method provides to execute a user command
		@param str_command an avaliable server management command in a string text format 
		@param arguments a list of arguments in a string text format
		@return a list of responses from a selected command in a string text format
	*/
	private List<String> process(String str_command, List<String> arguments)
	{
		//Start
		if(str_command.equalsIgnoreCase("Start")) 
		{
			return (new Start(email_manager)).execute();
		}
		
		//UserList
		else if(str_command.equalsIgnoreCase("UserList")) 
		{
			return (new UserList(ua_manager)).execute();
		}
		
		//ChangePOP3Port
		else if(str_command.equalsIgnoreCase("ChangePOP3Port")) 
		{
			if(arguments.size()==1)
			{
				if(arguments.size()==1)
				{
					String arg = arguments.get(0);
				
					if(isNumber(arg))
					{
						int port = toInteger(arg);
						return (new ChangePOP3Port(port)).execute();
					}
				}
			}
		}
		
		//ChangeSMTPPort
		else if(str_command.equalsIgnoreCase("ChangeSMTPPort")) 
		{
			if(arguments.size()==1)
			{
				String arg = arguments.get(0);
				
				if(isNumber(arg))
				{
					int port = toInteger(arg);
					return (new ChangeSMTPPort(port)).execute();
				}
			}
		}
		
		//ServerPort
		else if(str_command.equalsIgnoreCase("ServerPorts")) 
		{
			return (new ServerPorts(email_manager)).execute();
		}
		
		//DefaultPOP3Port
		else if(str_command.equalsIgnoreCase("DefaultPOP3Port")) 
		{
			return (new DefaultPOP3Port()).execute();
		}
		
		//DefaultSMTPPort
		else if(str_command.equalsIgnoreCase("DefaultSMTPPort")) 
		{
			return (new DefaultSMTPPort()).execute();
		}
		
		//Backup
		else if(str_command.equalsIgnoreCase("Backup")) 
		{
			return (new Backup(email_manager.getBackupFileManager())).execute();
		}
		
		//Restore
		else if(str_command.equalsIgnoreCase("Restore")) 
		{
			return (new Restore(email_manager.getBackupFileManager())).execute();
		}
		
		//ModifyDNS
		else if(str_command.equalsIgnoreCase("ModifyDNS")) 
		{
			if(arguments.size()==2)
			{
				String arg1 = arguments.get(0);
				String arg2 = arguments.get(1);
				
				if(!arg1.equals("") && !arg2.equals(""))
				{
					return (new ModifyDNS(arg1, arg2)).execute();
				}
			}
		}
		
		//AddDNS
		else if(str_command.equalsIgnoreCase("AddDNS")) 
		{
			if(arguments.size()==1)
			{
				String arg1 = arguments.get(0);
				
				if(!arg1.equals(""))
				{
					return (new AddDNS(arg1)).execute();
				}
			}
		}
		
		//RemoveDNS
		else if(str_command.equalsIgnoreCase("RemoveDNS")) 
		{
			if(arguments.size()==1)
			{
				String arg1 = arguments.get(0);
				
				if(!arg1.equals(""))
				{
					return (new RemoveDNS(arg1)).execute();
				}
			}
		}
		
		//RemoveAllUsers
		else if(str_command.equalsIgnoreCase("RemoveAllUsers")) 
		{
			return (new RemoveAllUsers(ua_manager)).execute();
		}
		
		//Shutdown
		else if(str_command.equalsIgnoreCase("Shutdown")) 
		{
			return (new Shutdown(email_manager)).execute();
		}
		
		//Restart
		else if(str_command.equalsIgnoreCase("Restart")) 
		{
			return (new Restart(email_manager)).execute();
		}
		
		//EnableAllUsers
		else if(str_command.equalsIgnoreCase("EnableAllUsers")) 
		{
			return (new EnableAllUsers(ua_manager)).execute();
		}
		
		//SuspendAllUsers
		else if(str_command.equalsIgnoreCase("SuspendAllUsers")) 
		{
			return (new SuspendAllUsers(ua_manager)).execute();
		}
		
		//EnableUser
		else if(str_command.equalsIgnoreCase("EnableUser")) 
		{
			if(arguments.size()==1)
			{
				String arg1 = arguments.get(0);
				
				if(!arg1.equals(""))
				{
					return (new EnableUser(ua_manager, arg1)).execute();
				}
			}
		}
		
		//RemoveUser
		else if(str_command.equalsIgnoreCase("RemoveUser")) 
		{
			if(arguments.size()==1)
			{
				String arg1 = arguments.get(0);
				
				if(!arg1.equals(""))
				{
					return (new RemoveUser(ua_manager, arg1)).execute();
				}
			}
		}
		
		//SuspendUser
		else if(str_command.equalsIgnoreCase("SuspendUser")) 
		{
			if(arguments.size()==1)
			{
				String arg1 = arguments.get(0);
				
				if(!arg1.equals(""))
				{
					return (new SuspendUser(ua_manager, arg1)).execute();
				}
			}
		}
		
		//AddUser
		else if(str_command.equalsIgnoreCase("AddUser")) 
		{
			if(arguments.size()==2)
			{
				String arg1 = arguments.get(0);
				String arg2 = arguments.get(1);
				
				if(!arg1.equals("") && !arg2.equals(""))
				{
					return (new AddUser(ua_manager, arg1, arg2).execute());
				}
			}
		}
		//ChangeUserPass
		else if(str_command.equalsIgnoreCase("ChangeUserPass")) 
		{
			if(arguments.size()==2)
			{
				String arg1 = arguments.get(0);
				String arg2 = arguments.get(1);
				
				if(!arg1.equals("") && !arg2.equals(""))
				{
					return (new ChangeUserPass(ua_manager, arg1, arg2).execute());
				}
			}
		}
		
		//Quit
		else if(str_command.equalsIgnoreCase("Quit")) 
		{
			return (new Quit()).execute();
		}
		
		ArrayList<String> unknown = new ArrayList<String>();
		unknown.add("Unknown command. Try again.");
		return unknown;
	}
	
	/**
		This method provides determine the string parameter is an Integer or not 
		@param str a string text
		@return true if the string input is in a numerical format
	*/
	private boolean isNumber(String str)
	{
		try
		{
			Integer.parseInt(str);
			return true;
		}
		catch(NumberFormatException ex) { }
		
		return false;
	}
	
	/**
		This method provides to convert a string parameter to an Integer
		@param str a string text in a number format
		@return an integer value
	*/
	private int toInteger(String str)
	{
		return Integer.parseInt(str);
	}
}
